# Authentification flow

The Symfony SecurityBundle provides all authentication and authorization features needed to secure your application.

The authentication mechanism is split into two parts:

- **Authentication**: the user is authenticated by the security system.
- **Authorization**: the user is authorized to access the application.

## Users

The users of the application are stored in the database into the table `users` which is mapped to the `User`
class (`App\Entity\User`).

The `User` entity MUST implement the `Symfony\Component\Security\Core\User\UserInterface`
and `Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface` interfaces to work properly.

The `User` entity have the following properties :

- `id`: the primary key of the entity.
- `username`: the username of the user.
- `password`: the password of the user.
- `email`: the email of the user.
- `roles`: the roles of the user.
- `tasks`: the tasks of the user.

The user have a unique `email` constraint which means that the email address must be unique in the database.

## Security config

The config file `security.yaml` located in `config/packages` is used to configure the Symfony security system.

```php
security:
    enable_authenticator_manager: true
    # https://symfony.com/doc/current/security.html#registering-the-user-hashing-passwords
    password_hashers:
        Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface: 'auto'
        App\Entity\User:
            algorithm: auto

        # https://symfony.com/doc/current/security.html#where-do-users-come-from-user-providers
    providers:
        # used to reload user from session & other features (e.g. switch_user)
        app_user_provider:
            entity:
                class: App\Entity\User
                property: username
    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        main:
            lazy: true
            form_login:
                # "login" is the name of the route created previously
                login_path: login
                check_path: login
            logout:
                path: logout
                # where to redirect after logout
                target: homepage

    # Easy way to control access for large sections of your site
    # Note: Only the *first* access control that matches will be used
    access_control:
        - { path: ^/login, roles: PUBLIC_ACCESS }
        - { path: ^/users, roles: ROLE_ADMIN }
        - { path: ^/, roles: IS_AUTHENTICATED_FULLY }
```

This files contains the following sections :

- password_hashers: the password algorithm used by the security system to hash passwords.

```php
password_hashers:
  Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface: 'auto'
  App\Entity\User:
  algorithm: auto
```

- providers: indicate where Symfony should look for the User entity and which User property is used to identify the
  user.

```php
providers:
        # used to reload user from session & other features (e.g. switch_user)
        app_user_provider:
            entity:
                class: App\Entity\User
                property: username 
```

- firewalls: prevent non-authenticated users from accessing certain parts of your site and define the name of the login
  and logout forms.

 ```php
firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        main:
            lazy: true
            form_login:
                # "login" is the name of the route created previously
                login_path: login
                check_path: login
            logout:
                path: logout
                # where to redirect after logout
                target: homepage
 ```

- access_control: control the access to the application.

```php
access_control:
        - { path: ^/login, roles: PUBLIC_ACCESS }
        - { path: ^/users, roles: ROLE_ADMIN }
        - { path: ^/, roles: IS_AUTHENTICATED_FULLY } 
```

The /users route is only accessible to users with the ROLE_ADMIN role which means that only admins can create and edit
Users