# Contributing

If you want to contribute to this project, please first create an issue to explain the change you wish to make.

## Gitlab

The code of this project is hosted on [Gitlab](https://gitlab.com/Pierre_t76/todo-and-co) and all code changes are made
by Merge Requests.

1. Clone the repository
2. Create a new branch with the name of you feature/bugfix
3. Write your code
4. Write tests
5. Update the README.md if needed
6. Ensure all tests pass
7. Create a Merge Request

## Coding Style

Make sure your code is well documented and follows the PSR-12 coding style. A Gitlab CI pipeline will run using phpcs to
check if your code follow these standards. You can fix the style of your code with PHP CS Fixer with the following
command:

```
composer beautify
```