# ToDoList

[![pipeline status](https://gitlab.com/Pierre_t76/todo-and-co/badges/main/pipeline.svg)](https://gitlab.com/Pierre_t76/todo-and-co/-/commits/main)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/9def378724a44832adfcc75e3544c923)](https://www.codacy.com/gl/Pierre_t76/todo-and-co/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=Pierre_t76/todo-and-co&amp;utm_campaign=Badge_Grade)

This project is carried out as part of my course PHP / Symfony Application Developer
at [OpenClassrooms](https://openclassrooms.com/fr/projects/ameliorer-un-projet-existant-1/assignment).

## Requirements 🔧

| Dependency | Minimum Version |
| ---------- | --------------- |
| PHP        | 8.0             |
| MySQL      | 5.7             |
| Composer   |                 |

## Project setup 📚

```bash
git clone https://gitlab.com/Pierre_t76/todo-and-co
```

```bash
cd todo-and-co
```

In the project folder execute this command :

```bash
cp .env .env.local
```

And then fill the environment variables into .env.local file.

```
APP_ENV=dev
APP_SECRET=generate-me

DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"
MAILER_DSN=smtp://localhost
```

You can now install the dependencies of the project with this command :

```bash
composer install
```

## Database setup 💾

To create the database and all the tables execute this command :

```bash
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
```

## Load data fixtures 📚

You can load data into your database with this command :

```bash
php bin/console doctrine:fixtures:load --no-interaction
```

## Run the application 🚀

To run the application in development mode you can use the Symfony built-in development server or the PHP built-in
server :

```bash
symfony server:start --no-tls
```

OR

```bash
php -S localhost:8000
```

## Test the application ✅

To run the tests you first need to setup the test environment :

```bash
cp .env.local .env.test.local
```

```bash
APP_ENV=test php bin/console doctrine:database:create
APP_ENV=test php bin/console doctrine:schema:update --force
composer recipes:install phpunit/phpunit --force -v
```

⚠️ This env variable should be present in your .env.test.local file :

```bash
KERNEL_CLASS=App\Kernel
```

You can adjust the database credentials in the .env.local.test file if you want. Then you can run this command which
will load Data fixtures and run the test suite.

Then you can run the tests with this command :

```bash
composer test
```

## Contribution 💪

See [Contribution Guide](./CONTRIBUTING.md).