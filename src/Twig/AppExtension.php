<?php

namespace App\Twig;

use App\Entity\User;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('author', [$this, 'displayAuthor']),
        ];
    }

    public function displayAuthor(?User $user): string
    {
        return $user?->getUsername() ?? 'Anonyme';
    }
}
