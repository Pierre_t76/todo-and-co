<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail('test@test.fr');
        $user->setUsername('test');
        $user->setPassword('test');
        $user->setRoles(['ROLE_USER']);

        $admin = new User();
        $admin->setEmail('admin@test.fr');
        $admin->setUsername('admin');
        $admin->setPassword('admin');
        $admin->setRoles(['ROLE_ADMIN']);

        $task = new Task();
        $task->setTitle('test');
        $task->setContent('test');
        $task->setUser($user);

        $task2 = new Task();
        $task2->setTitle('test');
        $task2->setContent('test');
        $task2->setUser($admin);

        $anonymousTask = new Task();
        $anonymousTask->setTitle('test');
        $anonymousTask->setContent('test');
        $anonymousTask->setUser(null);

        $manager->persist($user);
        $manager->persist($admin);
        $manager->persist($task);
        $manager->persist($task2);
        $manager->persist($anonymousTask);

        $manager->flush();
    }
}
