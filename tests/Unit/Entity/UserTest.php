<?php

namespace App\Tests\Unit\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private User $user;

    public function setUp(): void
    {
        $this->user = new User();
    }

    public function testDefaultRole(): void
    {
        $this->assertContains('ROLE_USER', $this->user->getRoles());
    }

    public function testMultipleRoles(): void
    {
        $this->user->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        $this->assertContains('ROLE_ADMIN', $this->user->getRoles());
    }

    public function testEmail(): void
    {
        $this->user->setEmail('test@test.fr');
        $this->assertEquals('test@test.fr', $this->user->getEmail());

        $this->assertSame($this->user->getEmail(), filter_var($this->user->getEmail(), FILTER_VALIDATE_EMAIL));
    }

    public function testUsername(): void
    {
        $this->user->setUsername('test');
        $this->assertEquals('test', $this->user->getUsername());
        $this->assertIsString($this->user->getUsername());
    }
}
