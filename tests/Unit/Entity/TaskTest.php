<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Task;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class TaskTest extends TestCase
{
    private Task $task;

    public function setUp(): void
    {
        $this->task = new Task();
    }

    public function testCreatedAt(): void
    {
        $this->assertInstanceOf(\DateTime::class, $this->task->getCreatedAt());
    }

    public function testSetCreatedAt(): void
    {
        $date = new \DateTime();

        $this->task->setCreatedAt($date);
        $this->assertSame($date->getTimestamp(), $this->task->getCreatedAt()->getTimestamp());
    }

    public function testNewTaskIsNotDone(): void
    {
        $this->assertFalse($this->task->isDone());
    }

    public function testSetUser(): void
    {
        $user = new User();

        $this->task->setUser($user);
        $this->assertSame($user, $this->task->getUser());
    }

    public function testToggleTask(): void
    {

        $this->task->toggle(true);
        $this->assertTrue($this->task->isDone());

        $this->task->toggle(false);
        $this->assertFalse($this->task->isDone());
    }

    public function testTitle(): void
    {
        $title = 'title';

        $this->task->setTitle($title);
        $this->assertSame($title, $this->task->getTitle());
        $this->assertIsString($this->task->getTitle());
    }

    public function testContent(): void
    {
        $content = 'content';

        $this->task->setContent($content);
        $this->assertSame($content, $this->task->getContent());
        $this->assertIsString($this->task->getContent());
    }

    public function testGetId(): void
    {
        $this->assertNull($this->task->getId());
    }
}
