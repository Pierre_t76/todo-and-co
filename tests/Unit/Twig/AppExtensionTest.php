<?php

namespace App\Tests\Unit\Twig;

use App\Entity\User;
use App\Twig\AppExtension;
use PHPUnit\Framework\TestCase;
use Twig\TwigFunction;

class AppExtensionTest extends TestCase
{
    protected AppExtension $extension;

    public function testExtensionIsLoaded(): void
    {
        $this->assertTrue(class_exists(AppExtension::class));
    }

    public function testDisplayAuthorIsDefined(): void
    {
        $this->assertTrue(method_exists($this->extension, 'displayAuthor'));
    }

    public function testDisplayAuthor(): void
    {
        $user = new User();
        $user->setUsername('username');

        $this->assertEquals('Anonyme', $this->extension->displayAuthor(null));
        $this->assertEquals($user->getUsername(), $this->extension->displayAuthor($user));
    }

    public function testGetFunctions(): void
    {
        $functions = $this->extension->getFunctions();
        $this->assertIsArray($functions);
        $this->assertCount(1, $functions);

        $this->assertInstanceOf(TwigFunction::class, $functions[0]);
        $this->assertEquals('author', $functions[0]->getName());
    }

    protected function setUp(): void
    {
        $this->extension = new AppExtension();
    }
}
