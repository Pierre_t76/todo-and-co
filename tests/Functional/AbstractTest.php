<?php


namespace App\Tests\Functional;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class AbstractTest extends WebTestCase
{
    protected KernelBrowser $client;
    protected UserRepository $userRepository;

    protected function loginUser(): void
    {
        $this->client->loginUser($this->userRepository->findOneBy(['username' => 'test']));
    }

    protected function loginAdmin(): void
    {
        $this->client->loginUser($this->userRepository->findOneBy(['username' => 'admin']));
    }

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->userRepository = self::getContainer()->get(UserRepository::class);
    }
}
