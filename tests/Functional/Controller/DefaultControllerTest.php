<?php

namespace App\Tests\Functional\Controller;

use App\Tests\Functional\AbstractTest;

class DefaultControllerTest extends AbstractTest
{
    public function testIndexAction(): void
    {
        $this->loginUser();
        $this->client->request('GET', '/');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->client->followRedirects();

        self::assertSelectorTextContains('h1', "Bienvenue sur Todo List, l'application vous permettant de gérer l'ensemble de vos tâches sans effort !");
    }

    public function testIndexActionWithoutAuthentication(): void
    {
        $this->client->request('GET', '/');
        self::assertResponseRedirects('http://localhost/login', 302);

        $crawler = $this->client->followRedirect();

        static::assertSame(1, $crawler->filter('input[name="_username"]')->count());
        static::assertSame(1, $crawler->filter('input[name="_password"]')->count());
    }
}
