<?php

namespace App\Tests\Functional\Controller;

use App\Entity\User;
use App\Tests\Functional\AbstractTest;

class UserControllerTest extends AbstractTest
{
    private int $userId;

    protected function setUp(): void
    {
        parent::setUp();
        $entityManager = self::getContainer()->get('doctrine');

        $this->userId = $entityManager->getRepository(User::class)
            ->findOneBy(['username' => 'test'])
            ->getId();
    }

    public function testListAction(): void
    {
        $this->client->request('GET', '/users');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->followRedirect();

        static::assertSame(1, $crawler->filter('input[name="_username"]')->count());
        static::assertSame(1, $crawler->filter('input[name="_password"]')->count());
    }

    public function testListActionAsUser(): void
    {
        $this->loginUser();
        $this->client->request('GET', '/users');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());
    }

    public function testListActionAsAdmin(): void
    {
        $this->loginAdmin();
        $crawler = $this->client->request('GET', '/users');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        static::assertSame('Liste des utilisateurs', $crawler->filter('h1')->text());
    }

    public function testCreateActionAsAdmin(): void
    {
        $this->loginAdmin();

        $this->client->request('GET', '/users/create');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->client->submitForm('Ajouter', [
            'user[username]' => 'user_test',
            'user[password][first]' => 'password',
            'user[password][second]' => 'password',
            'user[email]' => 'usertest@test.fr',
            'user[roles]' => 'ROLE_USER',
        ]);

        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $this->client->followRedirect();
        self::assertSelectorTextContains('div.alert-success', 'L\'utilisateur a bien été ajouté');
    }

    public function testCreateActionAsUser(): void
    {
        $this->loginUser();

        $this->client->request('GET', '/users/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());
    }

    public function testEditActionAsUser(): void
    {
        $this->loginUser();

        $this->client->request('GET', '/users/'.$this->userId.'/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());
    }

    public function testEditActionAsAdmin(): void
    {
        $this->loginAdmin();

        $this->client->request('GET', "/users/{$this->userId}/edit");
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->client->submitForm('Modifier', [
            'user[username]' => 'test',
            'user[password][first]' => 'newpassword',
            'user[password][second]' => 'newpassword',
            'user[email]' => 'test@test.fr',
            'user[roles]' => 'ROLE_USER',
        ]);

        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $this->client->followRedirect();
        self::assertSelectorTextContains('div.alert-success', 'L\'utilisateur a bien été modifié');
    }
}
