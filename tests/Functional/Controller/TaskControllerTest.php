<?php

namespace App\Tests\Functional\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Tests\Functional\AbstractTest;

class TaskControllerTest extends AbstractTest
{
    private int $userTaskIdAuthorized;
    private int $userTaskIdNotAuthorized;
    private int $anonymousTaskId;

    protected function setUp(): void
    {
        parent::setUp();
        $entityManager = self::getContainer()->get('doctrine');

        $this->userTaskIdAuthorized = $entityManager->getRepository(Task::class)
            ->findOneBy(['user' => $entityManager->getRepository(User::class)->findOneBy(['username' => 'test'])->getId()])
            ->getId();

        $this->userTaskIdNotAuthorized = $entityManager->getRepository(Task::class)
            ->findOneBy(['user' => $entityManager->getRepository(User::class)->findOneBy(['username' => 'admin'])->getId()])
            ->getId();

        $this->anonymousTaskId = $entityManager->getRepository(Task::class)
            ->findOneBy(['user' => null])
            ->getId();
    }

    public function testListAction(): void
    {
        $this->loginUser();
        $this->client->request('GET', '/tasks');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        self::assertSelectorTextContains('a.create-task', 'Créer une tâche');
    }

    public function testListActionWithoutAuthentication(): void
    {
        $this->client->request('GET', '/tasks');
        self::assertResponseRedirects('http://localhost/login', 302);

        $crawler = $this->client->followRedirect();
        $this->assertSame(
            2,
            $crawler->filter('form input#username')->count() + $crawler->filter('form input#password')->count()
        );
    }

    public function testCreateAction(): void
    {
        $this->loginUser();
        $this->client->request('GET', '/tasks/create');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->client->submitForm('Ajouter', [
            'task[title]' => 'Tache de test',
            'task[content]' => 'Contenu de la tache de test',
        ]);

        static::assertResponseRedirects('/tasks', 302);
        $crawler = $this->client->followRedirect();

        static::assertSame(200, $this->client->getResponse()->getStatusCode());
        self::assertSame('Superbe ! La tâche a été bien été ajoutée.', $crawler->filter('div.alert-success')->text());
    }

    public function testCreateActionWithoutAuthentication(): void
    {
        $this->client->request('GET', '/tasks/create');
        self::assertResponseRedirects('http://localhost/login', 302);

        $crawler = $this->client->followRedirect();
        $this->assertSame(
            2,
            $crawler->filter('form input#username')->count() + $crawler->filter('form input#password')->count()
        );
    }

    public function testEditActionAuthorized(): void
    {
        $this->loginUser();
        $this->client->request('GET', "/tasks/{$this->userTaskIdAuthorized}/edit");

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->client->submitForm('Modifier', [
            'task[title]' => 'Tache de test modifiée',
            'task[content]' => 'Contenu tâche de test modifiée',
        ]);

        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $this->client->followRedirect();
        self::assertSelectorTextContains('div.alert-success', 'La tâche a bien été modifiée');
    }

    public function testEditActionWithoutAuthentication(): void
    {
        $this->client->request('GET', "/tasks/{$this->userTaskIdAuthorized}/edit");
        $crawler = $this->client->followRedirect();

        $this->assertSame(
            2,
            $crawler->filter('form input#username')->count() + $crawler->filter('form input#password')->count()
        );
    }

    public function testToggleTaskAuthorized(): void
    {
        $this->loginUser();
        $this->client->request('GET', "/tasks/{$this->userTaskIdAuthorized}/toggle");

        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $this->client->followRedirect();

        self::assertSelectorTextContains('div.alert-success', 'Superbe ! La tâche Tache de test modifiée a bien été marquée comme faite.');
    }

    public function testToggleTaskWithoutAuthentication(): void
    {
        $this->client->request('GET', "/tasks/{$this->userTaskIdAuthorized}/toggle");

        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->followRedirect();
        $this->assertSame(
            2,
            $crawler->filter('form input#username')->count() + $crawler->filter('form input#password')->count()
        );
    }

    public function testDeleteTaskAuthorized(): void
    {
        $this->loginUser();
        $this->client->request('GET', "/tasks/{$this->userTaskIdAuthorized}/delete");

        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $this->client->followRedirect();

        self::assertSelectorTextContains('div.alert-success', 'Superbe ! La tâche a bien été supprimée.');
    }

    public function testDeleteTaskNotAuthorized(): void
    {
        $this->loginUser();
        $this->client->request('GET', "/tasks/{$this->userTaskIdNotAuthorized}/delete");

        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());
    }

    public function testDeleteTaskWithoutAuthentication(): void
    {
        $this->client->request('GET', "/tasks/{$this->userTaskIdAuthorized}/delete");
        $crawler = $this->client->followRedirect();

        $this->assertSame(
            2,
            $crawler->filter('form input#username')->count() + $crawler->filter('form input#password')->count()
        );
    }

    public function testDeleteAnonymousTaskAsUser(): void
    {
        $this->loginUser();
        $this->client->request('GET', "/tasks/{$this->anonymousTaskId}/delete");

        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());
    }

    public function testDeleteAnonymousTaskAsAdmin(): void
    {
        $this->loginAdmin();
        $this->client->request('GET', "/tasks/{$this->anonymousTaskId}/delete");

        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $this->client->followRedirect();

        self::assertSelectorTextContains('div.alert-success', 'Superbe ! La tâche a bien été supprimée.');
    }
}
