<?php

namespace App\Tests\Functional\Controller;

use App\Tests\Functional\AbstractTest;

class SecurityControllerTest extends AbstractTest
{

    public function testLogin(): void
    {
        $this->login('test', 'test');
        static::assertSame(302, $this->client->getResponse()->getStatusCode());
    }

    private function login(string $username, string $password): void
    {
        $this->client->request('GET', '/login');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        self::assertSelectorExists('input[name="_username"]');
        self::assertSelectorExists('input[name="_password"]');
        self::assertSelectorExists('button[type="submit"]');

        $this->client->submitForm('Se connecter', [
            '_username' => $username,
            '_password' => $password,
        ]);
    }

    public function testLoginWithWrongCredentials(): void
    {
        $this->login('test', 'wrong');
        $crawler = $this->client->followRedirect();

        static::assertSame(200, $this->client->getResponse()->getStatusCode());
        static::assertSame("Identifiants invalides.", $crawler->filter('div.alert.alert-danger')->text());
    }

    public function testLogoutCheck(): void
    {
        $this->login('test', 'test');
        $this->client->request('GET', '/logout');

        static::assertSame(302, $this->client->getResponse()->getStatusCode());
    }
}